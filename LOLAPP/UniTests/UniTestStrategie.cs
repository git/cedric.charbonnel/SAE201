using Models;

namespace Tests
{
    public class StrategieTests
    {
        [Fact]
        public void CreerStrategie_AvecNomDescriptionEtChampions_Valide()
        {
            // Arrange
            string nom = "Stratégie 1";
            string description = "Description de la stratégie";
            var champions = new List<Champion>
            {
                new Champion("Champion 1", "Titre 1", "Image 1"),
                new Champion("Champion 2", "Titre 2", "Image 2"),
                new Champion("Champion 3", "Titre 3", "Image 3"),
                new Champion("Champion 4", "Titre 4", "Image 4"),
                new Champion("Champion 5", "Titre 5", "Image 5")
            };

            // Act
            var strategie = new Strategie(nom, description, champions);

            // Assert
            Assert.Equal(nom, strategie.Name);
            Assert.Equal(description, strategie.Description);
            Assert.Equal(champions, strategie.Champions);
        }
    }
}
