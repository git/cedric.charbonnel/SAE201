using Models;

namespace Tests
{
    public class AbilityTests
    {
        [Fact]
        public void Ability_Constructor_SetsProperties()
        {
            // Arrange
            string name = "Capacit� 1";
            string image = "image1.png";
            string description = "Description de la capacit�";

            // Act
            var ability = new Ability(name, image, description);

            // Assert
            Assert.Equal(name, ability.Name);
            Assert.Equal(image, ability.Image);
            Assert.Equal(description, ability.Description);
        }
    }
}
