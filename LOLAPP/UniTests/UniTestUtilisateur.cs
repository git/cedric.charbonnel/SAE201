using Xunit;
using Moq;
using Models;

namespace Tests
{
    public class IPersistanceManagerTests
    {
        [Fact]
        public void ChargeDonne_ReturnsExpectedData()
        {
            // Arrange
            var mockPersistance = new Mock<IPersistanceManager>();
            var expectedChampions = new List<Champion>
            {
                new Champion("Champion 1", "Titre 1", "Image 1"),
                new Champion("Champion 2", "Titre 2", "Image 2"),
            };
            var expectedUtilisateurs = new List<Utilisateur>
            {
                new Utilisateur("Utilisateur 1"),
                new Utilisateur("Utilisateur 2"),
            };
            mockPersistance.Setup(p => p.Chargdon())
                           .Returns((expectedChampions, expectedUtilisateurs));
            var manager = new Manager(mockPersistance.Object);

            // Act
            manager.Chargdon();

            // Assert
            Assert.Equal(expectedChampions, manager._champions);
            Assert.Equal(expectedUtilisateurs, manager._utilisateur);
        }

        [Fact]
        public void Sauvdon_CallsPersistanceManagerSauvdonMethod()
        {
            // Arrange
            var mockPersistance = new Mock<IPersistanceManager>();
            var manager = new Manager(mockPersistance.Object);

            // Act
            manager.Sauvdon();

            // Assert
            mockPersistance.Verify(p => p.Sauvdon(manager._champions, manager._utilisateur), Times.Once);
        }
    }
}
