using Models;
using System.Collections.Generic;
using Xunit;

namespace Tests
{
    public class PersistanceManagerTests
    {
        [Fact]
        public void Chargdon_ReturnsChampionsAndUtilisateurs()
        {
            // Arrange
            IPersistanceManager persistanceManager = new PersistanceManager(); // Remplacez PersistanceManager par l'implémentation réelle de IPersistanceManager
            var expectedChampions = new List<Champion>(); // Définissez les données de test pour les champions
            var expectedUtilisateurs = new List<Utilisateur>(); // Définissez les données de test pour les utilisateurs

            // Act
            var (actualChampions, actualUtilisateurs) = persistanceManager.Chargdon();

            // Assert
            Assert.Equal(expectedChampions, actualChampions);
            Assert.Equal(expectedUtilisateurs, actualUtilisateurs);
        }

        [Fact]
        public void Sauvdon_SavesChampionsAndUtilisateurs()
        {
            // Arrange
            IPersistanceManager persistanceManager = new PersistanceManager(); // Remplacez PersistanceManager par l'implémentation réelle de IPersistanceManager
            var championsToSave = new List<Champion>(); // Définissez les données de test pour les champions à sauvegarder
            var utilisateursToSave = new List<Utilisateur>(); // Définissez les données de test pour les utilisateurs à sauvegarder

            // Act
            persistanceManager.Sauvdon(championsToSave, utilisateursToSave);

            // Assert - Vérifiez que les données ont été sauvegardées correctement
        }
    }
}
