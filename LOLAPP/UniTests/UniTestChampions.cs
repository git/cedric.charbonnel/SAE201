using Models;
using System.Collections.Generic;

namespace Tests
{
    public class ChampionTests
    {
        [Fact]
        public void Champion_ConstructorWithAbilities_SetsProperties()
        {
            // Arrange
            string name = "Champion 1";
            string titre = "Titre du champion";
            string image = "image1.png";
            var abilities = new List<Ability>()
            {
                new Ability("Capacit� 1", "image1.png", "Description de la capacit� 1"),
                new Ability("Capacit� 2", "image2.png", "Description de la capacit� 2")
            };

            // Act
            var champion = new Champion(name, titre, image, abilities);

            // Assert
            Assert.Equal(name, champion.Name);
            Assert.Equal(titre, champion.Titre);
            Assert.Equal(image, champion.Image);
            Assert.Equal(abilities, champion.Abilities);
        }

        [Fact]
        public void Champion_ConstructorWithoutAbilities_SetsProperties()
        {
            // Arrange
            string name = "Champion 2";
            string titre = "Titre du champion";
            string image = "image2.png";

            // Act
            var champion = new Champion(name, titre, image);

            // Assert
            Assert.Equal(name, champion.Name);
            Assert.Equal(titre, champion.Titre);
            Assert.Equal(image, champion.Image);
            Assert.Empty(champion.Abilities);
        }
    }
}