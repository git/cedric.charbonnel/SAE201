﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stub
{
    /// <summary>
    /// Cette classe représente une classe d'exemple utilisant l'interface IPersistanceManager.
    /// </summary>
    public class Stub : IPersistanceManager
    {
        /// <summary>
        /// Initialise une nouvelle instance de la classe Stub.
        /// </summary>
        public Stub() { }

        /// <summary>
        /// Charge les données de champions et d'utilisateurs.
        /// </summary>
        /// <returns>
        /// Un tuple contenant une liste de champions et une liste d'utilisateurs.
        /// </returns>
        public (List<Champion>, List<Utilisateur>) Chargdon()
        {
            List<Utilisateur> u = new List<Utilisateur>();
            List<Champion> c = new List<Champion>();
            List<Ability> a = new List<Ability>();

            // Création des capacités pour le champion Riven
            Ability c1a1 = new Ability("Runic Blade", "runicblade.png", "Les compétences de Riven chargent sa lame et ses attaques de base consomment ces charges pour infliger des dégâts supplémentaires.");
            Ability c1a2 = new Ability("Broken Wings", "brokenwings.png", "Riven frappe avec une série de coups. Cette compétence peut être réactivée trois fois dans un court laps de temps, le troisième coup repoussant les ennemis proches.");
            Ability c1a3 = new Ability("Ki Burst", "kiburst.png", "Riven libère une explosion de Ki, infligeant des dégâts et étourdissant les ennemis proches.");
            Ability c1a4 = new Ability("Valor", "valor.png", "Riven fait un pas en avant sur une courte distance et bloque les dégâts entrants.");
            Ability c1a5 = new Ability("Blade of the Exile", "bladeoftheexile.png", "Riven renforce son arme précieuse avec de l'énergie, ce qui lui confère des dégâts d'attaque et une portée supplémentaires. Pendant ce temps, elle obtient également la capacité d'utiliser Vent tranchant, une attaque à distance puissante, une fois.");
            a.Add(c1a1);
            a.Add(c1a2);
            a.Add(c1a3);
            a.Add(c1a4);
            a.Add(c1a5);
            Champion c1 = new Champion("Riven", "The Exile", "riven.jpg", a);
            c.Add(c1);
            a.Clear();

            // Création des capacités pour le champion Gnar
            Ability c2a1 = new Ability("Rage Gene", "ragegene.png", "Lorsqu'il est en combat, Gnar génère de la Rage. À la Rage maximale, sa prochaine compétence le transforme en Méga Gnar, lui conférant une survie accrue et de nouvelles compétences.");
            Ability c2a2 = new Ability("Boomerang Throw / Boulder Toss", "boomerangthrow.png", "Gnar lance un boomerang qui inflige des dégâts et ralentit les ennemis touchés avant de revenir vers lui. S'il attrape le boomerang, son temps de recharge est réduit.\r\n\r\nMéga Gnar lance plutôt un rocher qui s'arrête au premier ennemi touché, infligeant des dégâts et ralentissant tout ce qui se trouve à proximité. Il peut ensuite le ramasser pour réduire le temps de recharge.");
            Ability c2a3 = new Ability("Hyper / Wallop", "hyper.png", "Les attaques et les sorts de Gnar l'excitent, lui infligeant des dégâts supplémentaires et lui octroyant de la vitesse de déplacement.\r\n\r\nMéga Gnar est trop enragé pour être excité et peut plutôt se dresser sur ses pattes arrière et frapper violemment la zone devant lui, étourdissant les ennemis dans une zone.");
            Ability c2a4 = new Ability("Hop / Crunch", "hop.png", "Gnar saute vers une position et rebondit sur la tête de toute unité sur laquelle il atterrit, parcourant une plus grande distance.\r\n\r\nMéga Gnar est trop grand pour rebondir et atterrit plutôt avec une force dévastatrice, infligeant des dégâts dans une zone autour de lui.");
            Ability c2a5 = new Ability("GNAR!", "gnarr.png", "Méga Gnar projette tout ce qui l'entoure dans une direction choisie, infligeant des dégâts et les ralentissant. Tout ennemi qui heurte un mur est étourdi et subit des dégâts supplémentaires.");
            a.Add(c2a1);
            a.Add(c2a2);
            a.Add(c2a3);
            a.Add(c2a4);
            a.Add(c2a5);
            Champion c2 = new Champion("Gnar", "The Missing Link", "gnar.jpg", a);
            c.Add(c2);
            a.Clear();

            // Création des capacités pour le champion Samira
            Ability c3a1 = new Ability("Daredevil Impulse", "daredevilimpulse.png", "Samira accumule une combinaison en infligeant des attaques ou des compétences différentes de la précédente. Les attaques de Samira en mêlée infligent des dégâts magiques supplémentaires. Les attaques contre des ennemis affectés par des effets d'immobilisation la font se précipiter vers leur portée d'attaque. Si l'ennemi est projeté en l'air, elle le maintient brièvement en l'air.");
            Ability c3a2 = new Ability("Flair", "flair.png", "Samira tire un coup de feu ou balance son épée, infligeant des dégâts. Si utilisée pendant Ruée sauvage, elle frappe tous les ennemis sur son chemin à la fin.");
            Ability c3a3 = new Ability("Blade Whirl", "bladewhirl.png", "Samira tourbillonne autour d'elle, infligeant des dégâts aux ennemis et détruisant les projectiles ennemis.");
            Ability c3a4 = new Ability("Wild Rush", "wildrush.png", "Samira se précipite vers un ennemi (y compris les structures), frappant les ennemis qu'elle traverse et augmentant sa vitesse d'attaque. Tuer un champion ennemi réinitialise le temps de recharge de cette compétence.");
            Ability c3a5 = new Ability("Inferno Trigger", "infernotrigger.png", "Samira déchaîne un torrent de tirs de ses armes, tirant sauvagement sur tous les ennemis qui l'entourent.");
            a.Add(c3a1);
            a.Add(c3a2);
            a.Add(c3a3);
            a.Add(c3a4);
            a.Add(c3a5);
            Champion c3 = new Champion("Samira", "The Desert Rose", "samira.jpg", a);
            c.Add(c3);
            a.Clear();

            // Création des capacités pour le champion Yuumi
            Ability c4a1 = new Ability("Bop 'n' Block", "bopnblock.png", "Périodiquement, lorsque Yuumi attaque un champion, elle restaure du mana et obtient un bouclier qui la suit, la protégeant ainsi que l'allié auquel elle est attachée.");
            Ability c4a2 = new Ability("Prowling Projectile", "prowlingprojectile.png", "Yuumi tire un missile qui inflige des dégâts au premier ennemi touché. Il inflige des dégâts bonus et ralentit s'il met au moins 1 seconde pour atteindre sa cible.\r\n\r\nLorsqu'elle est attachée, le missile peut être contrôlé avec votre curseur.");
            Ability c4a3 = new Ability("You and Me!", "youandme.png", "Passivement, Yuumi augmente la force d'adaptation de son allié et de la sienne. Activement, Yuumi se précipite vers un allié ciblé, devenant insensible à tout sauf aux tourelles.");
            Ability c4a4 = new Ability("Zoomies", "zoomies.png", "Soigne Yuumi et augmente sa vitesse de déplacement et sa vitesse d'attaque. Si elle est attachée, elle le transmet à son allié à la place.");
            Ability c4a5 = new Ability("Final Chapter", "finalchapter.png", "Yuumi canalise sept vagues de dégâts, immobilisant quiconque est touché par au moins trois vagues. Yuumi peut se déplacer, s'attacher et lancer Zoomies! tout en canalisant.");
            a.Add(c4a1);
            a.Add(c4a2);
            a.Add(c4a3);
            a.Add(c4a4);
            a.Add(c4a5);
            Champion c4 = new Champion("Yuumi", "The Magical Cat", "yuumi.jpg", a);
            c.Add(c4);
            a.Clear();

            // Création des capacités pour le champion Volibear
            Ability c5a1 = new Ability("The Relentless Storm", "therelentlessstorm.png", "Les capacités de Volibear chargent sa griffe, et ses attaques de base consomment des charges pour infliger des dégâts supplémentaires.");
            Ability c5a2 = new Ability("Thundering Smash", "thunderingsmash.png", "Volibear déclenche une série de coups violents. Cette capacité peut être réactivée trois fois en peu de temps, et le troisième coup repousse les ennemis à proximité.");
            Ability c5a3 = new Ability("Frenzied Maul", "frenziedmaul.png", "Volibear inflige des dégâts supplémentaires et repousse légèrement sa cible avec une attaque de base après avoir utilisé une compétence.");
            Ability c5a4 = new Ability("Sky Splitter", "skysplitter.png", "Volibear plonge vers une cible avec une attaque foudroyante, infligeant des dégâts et ralentissant les ennemis proches. Il peut ensuite sauter sur un ennemi différent.");
            Ability c5a5 = new Ability("Stormbringer", "stormbringer.png", "Volibear fait appel à la foudre pour transformer et augmenter en taille. En tombant du ciel, il inflige des dégâts et ralentit les ennemis, puis les étourdit brièvement.");
            a.Add(c5a1);
            a.Add(c5a2);
            a.Add(c5a3);
            a.Add(c5a4);
            a.Add(c5a5);
            Champion c5 = new Champion("Volibear", "The Relentless Storm", "volibear.jpg", a);
            c.Add(c5);
            a.Clear();

            // Création des utilisateurs
            Utilisateur u1 = new Utilisateur("John Doe", "password");
            Utilisateur u2 = new Utilisateur("Jane Smith", "password");
            u.Add(u1);
            u.Add(u2);

            return (c, u);
        }

        public void Sauvdon(List<Champion> c, List<Utilisateur> u)
        {
            throw new NotImplementedException();
        }
    }
}