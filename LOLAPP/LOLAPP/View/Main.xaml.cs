using Models;
using System.Diagnostics;

namespace LOLAPP.View
{
    public partial class Main : ContentPage
    {
        /// <summary>
        /// Gestionnaire de champion.
        /// </summary>
        public Manager Mgr => (App.Current as App).MyManager;

        /// <summary>
        /// Constructeur de la classe Main.
        /// </summary>
        public Main()
        {
            InitializeComponent();
            BindingContext = Mgr;
        }

        /// <summary>
        /// Gestionnaire de l'�v�nement lorsqu'une image est tap�e.
        /// </summary>
        /// <param name="sender">L'objet d�clencheur de l'�v�nement.</param>
        /// <param name="e">Les arguments de l'�v�nement.</param>
        private void ImageTapped(object sender, EventArgs e)
        {
            Champion champ = null;
            var image = (Image)sender;
            foreach (var champion in Mgr._champions)
            {
                //Debug.WriteLine(image.Source.ToString());
                if ("File: " + champion.Image == image.Source.ToString())
                {
                    //Debug.WriteLine(champion.Image + " 2");
                    champ = champion;
                    break;
                }
            }
            //var champion = manager._champions.FirstOrDefault(c => c.Image == image.Source.ToString()); ;
            if (champ != null)
            {
                //Debug.WriteLine(champ.Name + " 3");
                // Mettre � jour les labels des d�tails du champion
                championNameLabel.Text = champ.Name;
                championTitreLabel.Text = champ.Titre;
                //Debug.WriteLine(champ.Abilities[0].Image);
                abilityimage1.Source = champ.Abilities[0].Image;
                abilityname1.Text = champ.Abilities[0].Name;
                abilitydesc1.Text = champ.Abilities[0].Description;
                abilityimage2.Source = champ.Abilities[1].Image;
                abilityname2.Text = champ.Abilities[1].Name;
                abilitydesc2.Text = champ.Abilities[1].Description;
                abilityimage3.Source = champ.Abilities[2].Image;
                abilityname3.Text = champ.Abilities[2].Name;
                abilitydesc3.Text = champ.Abilities[2].Description;
                abilityimage4.Source = champ.Abilities[3].Image;
                abilityname4.Text = champ.Abilities[3].Name;
                //Debug.WriteLine(champ.Abilities[3].Image);
                abilitydesc4.Text = champ.Abilities[3].Description;
                abilityimage5.Source = champ.Abilities[4].Image;
                abilityname5.Text = champ.Abilities[4].Name;
                abilitydesc5.Text = champ.Abilities[4].Description;
            }
        }
    }
}