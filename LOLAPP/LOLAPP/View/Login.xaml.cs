using Models;
using System.Diagnostics;

namespace LOLAPP.View
{
    public partial class Login : ContentPage
    {
        /// <summary>
        /// Gestionnaire de champion.
        /// </summary>
        public Manager Mgr => (App.Current as App).MyManager;

        /// <summary>
        /// Constructeur de la classe Login.
        /// </summary>
        public Login()
        {
            InitializeComponent();
            BindingContext = Mgr;
        }

        /// <summary>
        /// Gestionnaire de l'�v�nement lorsqu'on clique sur le bouton LoginUtilisateur.
        /// </summary>
        /// <param name="sender">L'objet d�clencheur de l'�v�nement.</param>
        /// <param name="e">Les arguments de l'�v�nement.</param>
        private void LoginUtilisateur_Clicked(object sender, EventArgs e)
        {
            string username = usernameEntry.Text;
            string password = passwordEntry.Text;

            // V�rifier si l'utilisateur existe et les informations de connexion sont valides
            Utilisateur utilisateur = Mgr._utilisateur.FirstOrDefault(u => u.Username == username && u.Password == password);
            if (utilisateur != null && Mgr.UtilisateurConnecte == null)
            {
                // Connexion r�ussie
                Mgr.UtilisateurConnecte = utilisateur;
                Logout.IsVisible = true;
                Suprim.IsVisible = true;
                DisplayAlert("Succ�s", "Connexion r�ussie", "OK");
            }
            else if (Mgr.UtilisateurConnecte != null)
            {
                // Utilisateur d�j� connect�
                DisplayAlert("Erreur", "Vous �tes d�j� connect�", "OK");
            }
            else
            {
                // Informations de connexion incorrectes
                passwordEntry.Text = "";
                DisplayAlert("Erreur", "Nom d'utilisateur ou mot de passe incorrect", "OK");
            }
            passwordEntry.Text = "";
        }

        /// <summary>
        /// Gestionnaire de l'�v�nement lorsqu'on clique sur le bouton LogoutUtilisateur.
        /// </summary>
        /// <param name="sender">L'objet d�clencheur de l'�v�nement.</param>
        /// <param name="e">Les arguments de l'�v�nement.</param>
        private void LogoutUtilisateur_Clicked(object sender, EventArgs e)
        {
            // V�rifier si l'utilisateur est connect�
            if (Mgr.UtilisateurConnecte != null)
            {
                // D�connexion r�ussie
                Mgr.UtilisateurConnecte = null;
                Logout.IsVisible = false;
                Suprim.IsVisible = false;
                DisplayAlert("Succ�s", "D�connexion r�ussie", "OK");
            }
            else
            {
                // Utilisateur non connect�
                DisplayAlert("Erreur", "Vous n'�tes pas connect�", "OK");
            }

            usernameEntry.Text = "";
            passwordEntry.Text = "";
        }

        /// <summary>
        /// Gestionnaire de l'�v�nement lorsqu'on clique sur le bouton SupprimerUtilisateur.
        /// </summary>
        /// <param name="sender">L'objet d�clencheur de l'�v�nement.</param>
        /// <param name="e">Les arguments de l'�v�nement.</param>
        private void SupprimerUtilisateur_Clicked(object sender, EventArgs e)
        {
            // V�rification de la connexion de l'utilisateur
            if (Mgr.UtilisateurConnecte != null)
            {
                // Suppression de l'utilisateur
                Mgr.RemoveUtilisateur(Mgr.UtilisateurConnecte);
                Mgr.UtilisateurConnecte = null;
                Mgr.Sauvdon();
                Logout.IsVisible = false;
                Suprim.IsVisible = false;
                DisplayAlert("Succ�s", "Compte supprim� avec succ�s", "OK");
            }
            else
            {
                // Utilisateur non trouv�
                DisplayAlert("Erreur", "Vous n'�tes pas connect�", "OK");
            }

            usernameEntry.Text = "";
            passwordEntry.Text = "";
        }
    }
}
