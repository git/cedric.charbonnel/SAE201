using Models;
using System.Diagnostics;

namespace LOLAPP.View
{
    public partial class Register : ContentPage
    {
        /// <summary>
        /// Gestionnaire d'utilisateur.
        /// </summary>
        public Manager Mgr => (App.Current as App).MyManager;

        /// <summary>
        /// Constructeur de la classe Register.
        /// </summary>
        public Register()
        {
            InitializeComponent();
            BindingContext = Mgr;
        }

        /// <summary>
        /// Gestionnaire de l'�v�nement lorsqu'on clique sur le bouton EnregistrerUtilisateur.
        /// </summary>
        /// <param name="sender">L'objet d�clencheur de l'�v�nement.</param>
        /// <param name="e">Les arguments de l'�v�nement.</param>
        private void EnregistrerUtilisateur_Clicked(object sender, EventArgs e)
        {
            string username = usernameEntry.Text;
            string password = passwordEntry.Text;

            if (Mgr._utilisateur.Any(u => u.Username == username))
            {
                DisplayAlert("Erreur", "Cet utilisateur existe d�j�", "OK");
                return;
            }

            Utilisateur utilisateur = new Utilisateur(username, password);
            Mgr.AddUtilisateur(utilisateur);
            Mgr.Sauvdon();

            DisplayAlert("Succ�s", "Utilisateur enregistr� avec succ�s", "OK");

            usernameEntry.Text = "";
            passwordEntry.Text = "";
        }
    }
}