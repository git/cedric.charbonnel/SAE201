using Models;
using System.Diagnostics;

namespace LOLAPP.Views.Content;

public partial class RegisterBouton : ContentView
{

    /// <summary>
    /// Liste des stratégies.
    /// </summary>
    public List<Strategie> StratList { get; set; }

    /// <summary>
    /// Gestionnaire d'utilisateur.
    /// </summary>
    public Manager Mgr => (App.Current as App).MyManager;

    /// <summary>
    /// Constructeur de la classe RegisterBouton.
    /// </summary>
	public RegisterBouton()
	{
		InitializeComponent();
        BindingContext = Mgr;
        if (Mgr.UtilisateurConnecte != null)
            {
                StratList = Mgr.UtilisateurConnecte._strat;
            }
            else
            {
                StratList = null;
            }
	}

    /// <summary>
    /// Gestionnaire de l'événement lorsqu'on clique sur le bouton AddButton.
    /// </summary>
    /// <param name="sender">L'objet déclencheur de l'événement.</param>
    /// <param name="e">Les arguments de l'événement.</param>
    private void AddButton_Clicked(object sender, EventArgs e)
    {
        // Créer une nouvelle instance de Strategie et l'ajouter à StratList
        List<Champion> champ = new List<Champion>();
        champ.Add(Mgr._champions[0]);
        champ.Add(Mgr._champions[1]);
        champ.Add(Mgr._champions[2]);
        champ.Add(Mgr._champions[3]);
        champ.Add(Mgr._champions[4]);
        Strategie nouvelleStrategie = new Strategie("test", "allllooo", champ);
        StratList.Add(nouvelleStrategie);
    }
}