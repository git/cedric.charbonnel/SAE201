using Models;
using System.Diagnostics;

namespace LOLAPP.Views.Content;

public partial class LoginBouton : ContentView
{
    /// <summary>
    /// Gestionnaire d'utilisateur.
    /// </summary>
    public Manager Mgr => (App.Current as App).MyManager;

    /// <summary>
    /// Constructeur de la classe LoginBouton.
    /// </summary>
	public LoginBouton()
	{
		InitializeComponent();
        BindingContext = Mgr;
	}

    /// <summary>
    /// Gestionnaire de l'événement lorsqu'on clique sur le bouton DeleteButton.
    /// </summary>
    /// <param name="sender">L'objet déclencheur de l'événement.</param>
    /// <param name="e">Les arguments de l'événement.</param>
    private void DeleteButton_Clicked(object sender, EventArgs e)
    {
        var button = (Button)sender;
        var strat = (Strategie)button.CommandParameter;

        // Gérez la suppression de la stratégie spécifiée ici
        // StratList.Remove(strat);
    }
}