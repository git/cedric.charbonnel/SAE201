using Models;
using System.Diagnostics;

namespace LOLAPP.View
{
    public partial class Strat : ContentPage
    {
        /// <summary>
        /// Liste des strat�gies.
        /// </summary>
        public List<Strategie> StratList { get; set; }

        /// <summary>
        /// Gestionnaire de champion et d'utilisateur.
        /// </summary>
        public Manager Mgr => (App.Current as App).MyManager;

        /// <summary>
        /// Constructeur de la classe Strat.
        /// </summary>
        public Strat()
        {
            InitializeComponent();

            // Initialisez la liste des strat�gies
            if (Mgr.UtilisateurConnecte != null)
            {
                StratList = Mgr.UtilisateurConnecte._strat;
            }
            else
            {
                StratList = null;
            }

            // D�finissez le contexte de liaison de la collection sur cette page
            BindingContext = this;
        }

        /// <summary>
        /// Gestionnaire de l'�v�nement lorsqu'on clique sur le bouton AddButton.
        /// </summary>
        /// <param name="sender">L'objet d�clencheur de l'�v�nement.</param>
        /// <param name="e">Les arguments de l'�v�nement.</param>
        private void AddButton_Clicked(object sender, EventArgs e)
        {
            // Cr�er une nouvelle instance de Strategie et l'ajouter � StratList
            List<Champion> champ = new List<Champion>();
            champ.Add(Mgr._champions[0]);
            champ.Add(Mgr._champions[1]);
            champ.Add(Mgr._champions[2]);
            champ.Add(Mgr._champions[3]);
            champ.Add(Mgr._champions[4]);
            Strategie nouvelleStrategie = new Strategie("test", "allllooo", champ);
            StratList.Add(nouvelleStrategie);

            // Faire d�filer la ListView vers le nouvel �l�ment ajout�
            stratListView.ScrollTo(nouvelleStrategie, ScrollToPosition.End, true);
            Mgr.Sauvdon();
        }

        /// <summary>
        /// Gestionnaire de l'�v�nement lorsqu'on clique sur le bouton DeleteButton.
        /// </summary>
        /// <param name="sender">L'objet d�clencheur de l'�v�nement.</param>
        /// <param name="e">Les arguments de l'�v�nement.</param>
        private void DeleteButton_Clicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var strat = (Strategie)button.CommandParameter;

            // G�rez la suppression de la strat�gie sp�cifi�e ici
            // StratList.Remove(strat);
            Mgr.Sauvdon();
        }
    }
}
