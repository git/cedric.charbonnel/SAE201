﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOLAPP.DataContractPersistance
{
    /// <summary>
    /// Classe de données utilisée pour la persistance des champions et des utilisateurs.
    /// </summary>
    public class DataToPersist
    {
        /// <summary>
        /// Liste des champions à persister.
        /// </summary>
        public List<Champion> champ { get; set; } = new List<Champion>();

        /// <summary>
        /// Liste des utilisateurs à persister.
        /// </summary>
        public List<Utilisateur> util { get; set; } = new List<Utilisateur>();
    }
}
