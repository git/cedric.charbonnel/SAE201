﻿using Models;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml;


namespace LOLAPP.DataContractPersistance
{
    /// <summary>
    /// Implémentation de l'interface IPersistanceManager utilisant la sérialisation DataContract.
    /// </summary>
    public class DataContract : IPersistanceManager
    {
        /// <summary>
        /// Chemin du répertoire de sauvegarde des fichiers.
        /// </summary>
        public string FilePath { get; set; } = FileSystem.Current.AppDataDirectory;

        /// <summary>
        /// Nom du fichier de sauvegarde des champions.
        /// </summary>
        public string FileNameC { get; set; } = "championsSave.xml";

        /// <summary>
        /// Nom du fichier de sauvegarde des utilisateurs.
        /// </summary>
        public string FileNameU { get; set; } = "utilisateurSave.xml";

        /// <summary>
        /// Charge les données des champions et des utilisateurs à partir des fichiers de sauvegarde.
        /// </summary>
        /// <returns>Un tuple contenant la liste des champions et des utilisateurs chargés.</returns>
        public (List<Champion>, List<Utilisateur>) Chargdon()
        {
            var champserializer = new DataContractSerializer(typeof(DataToPersist));
            var utilserializer = new DataContractSerializer(typeof(List<Utilisateur>));
            List<Utilisateur> util = new List<Utilisateur>();
            List<Champion> champ = new List<Champion>();
            DataToPersist data;

            if (File.Exists(Path.Combine(FilePath, FileNameC))) // Vérifiez si le fichier existe
            {
                using (Stream champstream = File.OpenRead(Path.Combine(FilePath, FileNameC))) 
                { 
                    data = champserializer.ReadObject(champstream) as DataToPersist;
                    champ = data.champ; 
                }
            }
            else
            {
                data = new DataToPersist(); // Si le fichier n'existe pas, créez une nouvelle liste
            }

            if (File.Exists(Path.Combine(FilePath, FileNameU))) // Vérifiez si le fichier existe
            {
                using (Stream utilstream = File.OpenRead(Path.Combine(FilePath, FileNameU)))
                {
                    util = utilserializer.ReadObject(utilstream) as List<Utilisateur>;
                }
            }
            else
            {
                util = new List<Utilisateur>(); // Si le fichier n'existe pas, créez une nouvelle liste
            }
            return (champ, util);
        }

        /// <summary>
        /// Sauvegarde les données des champions et des utilisateurs dans des fichiers séparés.
        /// </summary>
        /// <param name="c">La liste des champions à sauvegarder.</param>
        /// <param name="u">La liste des utilisateurs à sauvegarder.</param>
        public void Sauvdon(List<Champion> c, List<Utilisateur> u)
        {
            var champserializer = new DataContractSerializer(typeof(DataToPersist));
            var utilserializer = new DataContractSerializer(typeof(List<Utilisateur>));

            if (!Directory.Exists(FilePath))
            {
                Debug.WriteLine("Directory créé à l'instant");
                Debug.WriteLine(Directory.GetDirectoryRoot(FilePath));
                Debug.WriteLine(FilePath);
                Directory.CreateDirectory(FilePath);
            }

            DataToPersist data = new DataToPersist();
            data.champ= c;

            using (Stream champstream = File.Create(Path.Combine(FilePath, FileNameC)))
            {
                champserializer.WriteObject(champstream,data);
            }

            using (Stream utilstream = File.Create(Path.Combine(FilePath, FileNameU)))
            {
                utilserializer.WriteObject(utilstream, u);
            }
        }
    }
}
