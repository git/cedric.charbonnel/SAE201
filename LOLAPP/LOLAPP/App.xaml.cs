﻿using LOLAPP.DataContractPersistance;
using Models;
using Stub;
using System.Collections.Generic;
using System.Diagnostics;

namespace LOLAPP;

public partial class App : Application
{

	public Manager MyManager { get; private set; } = new Manager(new Stub.Stub());
    public string FileNameC { get; set; } = "champions.xml";
    public string FileNameU { get; set; } = "utilisateur.xml";
    public string FilePath { get; set; } = FileSystem.AppDataDirectory;

    public App()
	{
        InitializeComponent();
        string champPath = Path.Combine(FilePath, FileNameC);
        string utilPath = Path.Combine(FilePath, FileNameU);
        /*Si les fichiers existent déjà, on récupère la persistance car on a donc des champions à récupérer*/
        if (File.Exists(champPath) && File.Exists(utilPath))
        {
            MyManager = new Manager(new DataContractPersistance.DataContract());
        }

        MyManager.Chargdon();

        MainPage = new AppShell();

        if (!File.Exists(champPath) && !File.Exists(utilPath))
        {
            MyManager.Persistance = new DataContractPersistance.DataContract();
        }

        MyManager.Sauvdon();

        Debug.WriteLine("Application lancééé!");
	}
}