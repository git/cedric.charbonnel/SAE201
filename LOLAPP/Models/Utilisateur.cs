﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    /// <summary>
    /// Représente un utilisateur.
    /// </summary>
    public class Utilisateur
    {
        [DataMember]
        /// <summary>
        /// Définit le nom d'utilisateur de l'utilisateur.
        /// </summary>
        public string Username { get; private set; }

        [DataMember]
        /// <summary>
        /// Définit le mot de passe de l'utilisateur.
        /// </summary>
        public string Password { get; private set; }

        [DataMember]
        /// <summary>
        /// Définit la liste de stratégies de l'utilisateur.
        /// </summary>
        public List<Strategie> _strat { get; private set; }

        /// <summary>
        /// Initialise une nouvelle instance de la classe Utilisateur avec un nom d'utilisateur, un mot de passe et une liste de stratégies.
        /// </summary>
        /// <param name="username">Le nom d'utilisateur</param>
        /// <param name="password">Le mot de passe</param>
        /// <param name="strat">La liste de stratégies</param>
        public Utilisateur(string username, string password, List<Strategie> strat)
        {
            Username = username;
            Password = password;
            _strat = strat;
        }

        /// <summary>
        /// Initialise une nouvelle instance de la classe Utilisateur avec un nom d'utilisateur et un mot de passe.
        /// La liste de stratégies est initialisée avec une liste vide.
        /// </summary>
        /// <param name="username">Le nom d'utilisateur</param>
        /// <param name="password">Le mot de passe</param>
        public Utilisateur(string username, string password)
        {
            Username = username;
            Password = password;
            _strat = new List<Strategie>();
        }

        /// <summary>
        /// Ajoute une stratégie à la liste des stratégies de l'utilisateur.
        /// </summary>
        /// <param name="strat">La stratégie à ajouter</param>
        public void AddStrategie(Strategie strat)
        {
            _strat.Add(strat);
        }

        /// <summary>
        /// Supprime une stratégie de la liste des stratégies de l'utilisateur.
        /// </summary>
        /// <param name="strat">La stratégie à supprimer</param>
        public void RemoveStrategie(Strategie strat)
        {
            _strat.Remove(strat);
        }
    }
}
