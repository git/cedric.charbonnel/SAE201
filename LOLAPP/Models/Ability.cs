﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    /// <summary>
    /// Représente une capacité d'un champion.
    /// </summary>
    public class Ability
    {
        [DataMember]
        /// <summary>
        /// Définit le nom de l'abilité.
        /// </summary>
        public string Name { get; private set; }

        [DataMember]
        /// <summary>
        /// Définit le nom du fichier image associé à l'abilité.
        /// </summary>
        public string Image { get; private set; }

        [DataMember]
        /// <summary>
        /// Définit la description de l'abilité.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Initialise une nouvelle instance de la classe Ability avec le nom, l'image et la description spécifiés.
        /// </summary>
        /// <param name="name">Le nom de l'abilité.</param>
        /// <param name="image">Le nom du fichier image associé à l'abilité.</param>
        /// <param name="description">La description de l'abilité.</param>
        public Ability(string name, string image, string description)
        {
            Name = name;
            Image = image;
            Description = description;
        }
    }

}
