﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// Représente une stratégie dans le système.
    /// </summary>
    [DataContract]
    public class Strategie
    {
        [DataMember]
        /// <summary>
        /// Définit le nom de la stratégie.
        /// </summary>
        public string Name { get; private set; }

        [DataMember]
        /// <summary>
        /// Définit la description de la stratégie.
        /// </summary>
        public string Description { get; private set; }

        [DataMember]
        /// <summary>
        /// Définit la liste des champions associés à la stratégie.
        /// </summary>
        public List<Champion> Champions { get; private set; }

        /// <summary>
        /// Initialise une nouvelle instance de la classe Strategie avec un nom, une description et une liste de champions.
        /// </summary>
        /// <param name="name">Le nom de la stratégie</param>
        /// <param name="description">La description de la stratégie</param>
        /// <param name="champions">La liste des champions associés à la stratégie</param>
        public Strategie(string name, string description, List<Champion> champions)
        {
            Name = name;
            Description = description;
            Champions = champions;
        }
    }
}
