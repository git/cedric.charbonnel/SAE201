﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Models
{
    /// <summary>
    /// Gère les opérations liées aux champions et aux utilisateurs dans le système.
    /// </summary>
    public class Manager
    {
        /// <summary>
        /// Définit la liste des champions.
        /// </summary>
        public List<Champion> _champions { get; private set; }

        /// <summary>
        /// Définit la liste des utilisateurs.
        /// </summary>
        public List<Utilisateur> _utilisateur { get; private set; }

        /// <summary>
        /// Définit le gestionnaire de persistance utilisé pour sauvegarder et charger les données.
        /// </summary>
        public IPersistanceManager? Persistance { get; set; }

        /// <summary>
        /// Définit l'utilisateur connecté. Utilisé pour la fonctionnalité de connexion et d'inscription.
        /// </summary>
        public Utilisateur? UtilisateurConnecte { get; set; } = null; //zzrajouté pour login register

        /// <summary>
        /// Initialise une nouvelle instance de la classe Manager avec un gestionnaire de persistance.
        /// </summary>
        /// <param name="Pers">Le gestionnaire de persistance</param>
        public Manager(IPersistanceManager Pers)
        {
            _champions = new List<Champion>();
            _utilisateur = new List<Utilisateur>();
            Persistance = Pers;
        }

        /// <summary>
        /// Initialise une nouvelle instance de la classe Manager.
        /// </summary>
        public Manager()
        {
            _champions = new List<Champion>();
            _utilisateur = new List<Utilisateur>();
        }

        /// <summary>
        /// Ajoute un utilisateur à la liste des utilisateurs.
        /// </summary>
        /// <param name="utilisateur">L'utilisateur à ajouter</param>
        public void AddUtilisateur(Utilisateur utilisateur)
        {
            _utilisateur.Add(utilisateur);
        }

        /// <summary>
        /// Supprime un utilisateur de la liste des utilisateurs.
        /// </summary>
        /// <param name="utilisateur">L'utilisateur à supprimer</param>
        public void RemoveUtilisateur(Utilisateur utilisateur)
        {
            _utilisateur.Remove(utilisateur);
        }

        /// <summary>
        /// Charge les données à partir de la source de persistance.
        /// </summary>
        public void Chargdon()
        {
            var don = Persistance!.Chargdon();

            _champions.AddRange(don.Item1);
            _utilisateur.AddRange(don.Item2);
        }

        /// <summary>
        /// Sauvegarde les données dans la source de persistance.
        /// </summary>
        public void Sauvdon()
        {
            Persistance?.Sauvdon(_champions, _utilisateur);
        }
    }
}
