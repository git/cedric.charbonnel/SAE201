﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// Définit les opérations de persistance pour sauvegarder et charger les données.
    /// </summary>
    public interface IPersistanceManager
    {
        /// <summary>
        /// Charge les données à partir de la source de persistance.
        /// </summary>
        /// <returns>Un tuple contenant la liste des champions chargés et la liste des utilisateurs chargés</returns>
        (List<Champion>, List<Utilisateur>) Chargdon();

        /// <summary>
        /// Sauvegarde les données dans la source de persistance.
        /// </summary>
        /// <param name="c">La liste des champions à sauvegarder</param>
        /// <param name="u">La liste des utilisateurs à sauvegarder</param>
        void Sauvdon(List<Champion> c, List<Utilisateur> u);
    }
}
