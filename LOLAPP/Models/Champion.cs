﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [DataContract]
    /// <summary>
    /// Représente un champion.
    /// </summary>
    public class Champion
    {
        /// <summary>
        /// Définit ou définit le nom du champion.
        /// </summary>
        [DataMember]
        public string Name { get; private set; }

        /// <summary>
        /// Définit ou définit le titre du champion.
        /// </summary>
        [DataMember]
        public string Titre { get; private set; }

        /// <summary>
        /// Définit ou définit le nom de l'image associée au champion.
        /// </summary>
        [DataMember]
        public string Image { get; private set; }

        /// <summary>
        /// Définit ou définit la liste des abilités du champion.
        /// </summary>
        [DataMember]
        public List<Ability> Abilities { get; private set; }

        /// <summary>
        /// Initialise une nouvelle instance de la classe Champion avec un nom, un titre, une image et une liste d'abilités.
        /// </summary>
        /// <param name="name">Le nom du champion</param>
        /// <param name="titre">Le titre du champion</param>
        /// <param name="image">Le nom de l'image associée au champion</param>
        /// <param name="abilities">La liste des abilités du champion</param>
        public Champion(string name, string titre, string image, List<Ability> abilities)
        {
            Name = name;
            Titre = titre;
            Image = image;
            Abilities = new List<Ability>(abilities);
        }

        /// <summary>
        /// Initialise une nouvelle instance de la classe Champion avec un nom, un titre et une image.
        /// La liste des abilités est initialisée avec une liste vide.
        /// </summary>
        /// <param name="name">Le nom du champion</param>
        /// <param name="titre">Le titre du champion</param>
        /// <param name="image">Le nom de l'image associée au champion</param>
        public Champion(string name, string titre, string image)
        {
            Name = name;
            Titre = titre;
            Image = image;
            Abilities = new List<Ability>();
        }
    }
}